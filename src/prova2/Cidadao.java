
package prova2;


public class Cidadao extends Usuario {
    
     private String cpf;
     private Endereco endereco;
     
    public Cidadao(String nome, String id, String cpf){
        
       super(nome,id);
       this.cpf = cpf;
    }


    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public Endereco getEndereco() {
        return endereco;
    }

    public void setEndereco(Endereco endereco) {
        this.endereco = endereco;
    }
    
    
}
