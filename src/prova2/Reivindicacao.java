
package prova2;

import java.util.ArrayList;

public class Reivindicacao {
   
    private String titulo;
    private String texto;
    private ArrayList<Comentario> listacomentario;
    
    //Construtor
     public Reivindicacao(String titulo, String texto){
        this.titulo = titulo;
        this.texto = texto;
    }

     
     
     
    public ArrayList<Comentario> getListacomentario() {
        return listacomentario;
    }

    public void setListacomentario(ArrayList<Comentario> listacomentario) {
        this.listacomentario = listacomentario;
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }
    
     
}
