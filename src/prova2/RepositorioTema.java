
package prova2;

import java.util.ArrayList;


public class RepositorioTema {
    
     private ArrayList<Tema> listaTemas;
    
    public RepositorioTema(){
       listaTemas = new ArrayList<Tema>();
    }
    
    public String adicionar(Tema titulo){
             listaTemas.add(titulo);
            return "Tema adicionado com sucesso!";
        }
         
          public String remover(Tema titulo){
            listaTemas.remove(titulo);
            return "Tema removido com sucesso!";
        }
         
         public Tema pesquisar(String titulo){
		for(Tema texto: listaTemas){
			if(texto.getNome().equalsIgnoreCase(titulo)){
				return texto;
			}
		}
		return null;
		}
    
    
}
