                                                                                                                                                                                                                                                                                                                             
package prova2;

import java.util.ArrayList;


public class Usuario {
    
    private String nome;
    private String id;
    private ArrayList<Reivindicacao> listaReivindicacao;
    
    //Construtor
    public Usuario(String nome, String id){
        this.nome = nome;
        this.id = id;
    }
    
    

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public ArrayList<Reivindicacao> getListaReivindicacao() {
        return listaReivindicacao;
    }

    public void setListaReivindicacao(ArrayList<Reivindicacao> listaReivindicacao) {
        this.listaReivindicacao = listaReivindicacao;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

      
}
