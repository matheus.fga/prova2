
package prova2;

import java.util.ArrayList;

public class Tema {
    
    private String nome;
    private int categoria;
    private ArrayList<Reivindicacao> listaReivindicacao;
    
     //Construtor
    public Tema(String nome, int categoria){
        this.nome = nome;
        this.categoria = categoria;  
    }
    
    

    public int getCategoria() {
        return categoria;
    }

    public void setCategoria(int categoria) {
        this.categoria = categoria;
    }

    public ArrayList<Reivindicacao> getListaReivindicacao() {
        return listaReivindicacao;
    }

    public void setListaReivindicacao(ArrayList<Reivindicacao> listaReivindicacao) {
        this.listaReivindicacao = listaReivindicacao;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
    
   
}
